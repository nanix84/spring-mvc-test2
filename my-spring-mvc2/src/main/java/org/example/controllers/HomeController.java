package org.example.controllers;

import org.example.dao.ProductDao;
import org.example.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@Autowired
	private ProductDao productDao;

    @RequestMapping("/")
    public String greeting(Model model) {
        model.addAttribute("name", "World!");
        return "index";
    }
    
    /* must uncomment spring.jpa.properties.hibernate.hbm2ddl.import_files
     * So initial data will be persisted or run jUnit HomeControllerTest to populate
     */
    @RequestMapping("/product/{id}")
    public String displayProduct(Model model, @PathVariable("id") String productId) {
    	Product p = null;
    	if(productId != null)
    		productDao.findOne(Integer.parseInt(productId));
    	model.addAttribute("name", "World!");
    	model.addAttribute("product", p);
    	return "index";
    }
}
