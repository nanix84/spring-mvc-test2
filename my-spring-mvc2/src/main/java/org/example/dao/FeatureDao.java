package org.example.dao;

import javax.transaction.Transactional;

import org.example.models.Feature;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface FeatureDao extends CrudRepository<Feature, Integer>{
	
	public Feature findByName(String name);
}
