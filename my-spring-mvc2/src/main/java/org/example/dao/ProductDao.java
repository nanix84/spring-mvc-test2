package org.example.dao;

import javax.transaction.Transactional;

import org.example.models.Product;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface ProductDao extends CrudRepository<Product, Integer> {
	
	public Product findByName(String name);
}
