package org.example.dao;

import javax.transaction.Transactional;

import org.example.models.ProductFeature;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface ProductFeatureDao extends CrudRepository<ProductFeature, Integer>{
	
}
