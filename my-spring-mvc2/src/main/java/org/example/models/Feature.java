package org.example.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "feature", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"name"})
})
public class Feature {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column( name = "feature_id" )
    private Integer id;
	
    private String name;
    
    @Enumerated(EnumType.STRING)
    private FeatureDataType dataType;
    
    public Feature() {}
    
    public Feature(Integer id, String name, FeatureDataType dataType) {
        this.id = id;
        this.name = name;
        this.dataType = dataType;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FeatureDataType getDataType() {
		return dataType;
	}

	public void setDataType(FeatureDataType dataType) {
		this.dataType = dataType;
	}
}
