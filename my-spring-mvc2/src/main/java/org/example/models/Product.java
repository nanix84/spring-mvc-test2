package org.example.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column( name = "product_id" )
    private Integer id;
	
    private String name;
    
    @OneToMany( mappedBy = "product", fetch = FetchType.LAZY )
    private Set<ProductFeature> productFeature;
    
    public Product() {}
    
    public Product(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ProductFeature> getProductFeature() {
		return productFeature;
	}

	public void setProductFeature(Set<ProductFeature> productFeature) {
		this.productFeature = productFeature;
	}
}
