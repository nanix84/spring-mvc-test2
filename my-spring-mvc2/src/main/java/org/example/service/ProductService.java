package org.example.service;

import org.example.models.Product;

public interface ProductService {
	
	public Product getProductById(String productId);
}
