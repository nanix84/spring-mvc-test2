package org.example.service;

import org.example.dao.ProductDao;
import org.example.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service( "productService" )
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductDao productDao;

	@Override
	public Product getProductById(String productId) {
		return productDao.findOne(Integer.parseInt(productId));
	}

}
