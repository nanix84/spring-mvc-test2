INSERT INTO product (product_id, name) VALUES (1, "Camera A");
INSERT INTO product (product_id, name) VALUES (2, "Camera B");
INSERT INTO product (product_id, name) VALUES (3, "Camera C");

INSERT INTO feature (feature_id, name, data_type) VALUES (1, "megapixels", "NUMERICAL");
INSERT INTO feature (feature_id, name, data_type) VALUES (2, "description", "STRING");

INSERT INTO product_feature (product_feature_id, product_id, feature_id, feature_value) VALUES (1, 1, 1, "10");

INSERT INTO product_feature (product_feature_id, product_id, feature_id, feature_value) VALUES (2, 1, 2, "This is camera A description");

INSERT INTO product_feature (product_feature_id, product_id, feature_id, feature_value) VALUES (3, 2, 1, "16");

INSERT INTO product_feature (product_feature_id, product_id, feature_id, feature_value) VALUES (4, 2, 2, "This is camera B description");

INSERT INTO product_feature (product_feature_id, product_id, feature_id, feature_value) VALUES (5, 3, 1, "20");

INSERT INTO product_feature (product_feature_id, product_id, feature_id, feature_value) VALUES (6, 3, 2, "This is camera C description");