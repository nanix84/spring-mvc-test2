package org.example.test.dao;

import static org.junit.Assert.assertEquals;

import javax.transaction.Transactional;

import org.example.Application;
import org.example.dao.FeatureDao;
import org.example.models.Feature;
import org.example.models.FeatureDataType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class FeatureDaoTest {
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Autowired
	private FeatureDao featureDao;
	
	@Before
	public void setup() {
		featureDao.deleteAll();
	}
	
	@Test
	@Rollback(true)
	public void testAddFeature() {
		Feature f = new Feature(1, "megapixels", FeatureDataType.NUMERICAL);
		// insert new feature
		featureDao.save(f);
		assertEquals(1, featureDao.count());
		// insert another feature with same feature name, different DataType
		f = new Feature();
		f.setName("megapixels");
		f.setDataType(FeatureDataType.STRING);
		// since you cannot insert new feature with the same name
		exception.expect(DataIntegrityViolationException.class);
		featureDao.save(f);
		assertEquals(1, featureDao.count());
	}
}
