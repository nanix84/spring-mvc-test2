package org.example.test.dao;

import static org.junit.Assert.*;

import javax.transaction.Transactional;

import org.example.Application;
import org.example.dao.ProductDao;
import org.example.models.Product;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class ProductDaoTest {
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Autowired
	private ProductDao productDao;
	
	@Test
	public void testAddProduct() {
		Product p = new Product(1, "Camera A");
		productDao.save(p);
		assertEquals(1, productDao.count());
	}
}
