package org.example.test.dao;

import static org.junit.Assert.*;

import javax.transaction.Transactional;

import org.example.Application;
import org.example.dao.FeatureDao;
import org.example.dao.ProductDao;
import org.example.dao.ProductFeatureDao;
import org.example.models.Feature;
import org.example.models.FeatureDataType;
import org.example.models.Product;
import org.example.models.ProductFeature;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class ProductFeatureDaoTest {
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Autowired
	private ProductFeatureDao productFeatureDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private FeatureDao featureDao;
	
	@Test
	public void testAddProductFeature() {
		Product p = new Product(null, "camera A");
		productDao.save(p);
		Feature f = new Feature(null, "megapixels", FeatureDataType.NUMERICAL);
		featureDao.save(f);
		ProductFeature pf = new ProductFeature(null, p, f, "18");
		productFeatureDao.save(pf);
		assertEquals(1, productFeatureDao.count());
		// insert another product feature with the same product and feature
		pf = new ProductFeature(null, p, f, "test description");
		// since you cannot insert the same product with the same feature this throws an exception
		exception.expect(DataIntegrityViolationException.class);
		productFeatureDao.save(pf);
		assertEquals(1, productFeatureDao.count());
		// insert a new Product with the same feature
		Product camB = new Product(null, "camera B");
		productDao.save(camB);
		pf = new ProductFeature(null, camB, f, "20");
		productFeatureDao.save(pf);
		assertEquals(2, productFeatureDao.count());
		// insert a Product 'camera A' with another feature 'description'
		f = new Feature(null, "description", FeatureDataType.STRING);
		featureDao.save(f);
		pf = new ProductFeature(null, p, f, "Test description");
		productFeatureDao.save(pf);
		assertEquals(3, productFeatureDao.count());
	}
}